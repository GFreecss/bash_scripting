#!/bin/bash

echo "This script prints the computer processes for you user."

while true
do
	read -p "This script prints the computer processes for your user. Do you want to sort the results by memory or by CPU consumption? " user_choice
	if [ $user_choice = "memory" ]
	then
		read -p "How many results do you want to print? " head_num
		result=$(ps aux --sort=pmem | grep $USER | head -$head_num)
		echo "$result"
		echo "Result was sorted by memory consumption"
		break
	elif [ $user_choice = "cpu" ]
	then
		read -p "How many results do you want to print? " head_num
		result=$(ps aux --sort=cpu | grep $USER | head -$head_num)
		echo "$result"
		echo "Result was sorted by cpu consumption"
		break
	else
		echo "$user_choice is not a possible answer. Please type: 'memory' or 'cpu'"
	fi
done
