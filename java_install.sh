#!/bin/bash

# Install Java JDK version 17
sudo apt update
sudo apt install openjdk-17-jdk-headless -y

java_version=$(java -version 2>&1 >/dev/null | grep "java version\|openjdk version" | awk '{print substr($3,2,2)}')

if [ $java_version -lt 11 ]; then
	echo "Your java version ($java_version) is not updated. Please do it"
elif [ $java_version -eq 11 ]; then
  echo "Your java version ($java_version) is the minimum version required. You can updated it if you want."
else
	echo "Your java version ($java_version) is updated. Go ahead!"
fi
