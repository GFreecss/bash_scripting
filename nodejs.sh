#!/bin/bash

# Install dependencies (nodejs and npm)
#is_nodejs=$(dpkg -l | awk '{print substr($2,0)}' | grep -P '(|\s)\Knodejs(?=s|$)')
#is_npm=$(dpkg -l | awk '{print substr($2,0)}' | grep -P '(|\s)\Knpm(?=s|$)')
NODE_USER=myapp
apt-get update > /dev/null
apt-get install -y nodejs npm > /dev/null
# Ensure nodejs installation
#if [ $is_nodejs = '' ]; then
#	echo "You don't have nodejs installed. Installing..."
#	apt-get update > /dev/null
#	apt-get install -y nodejs
#	sleep 1
#	node_version=$(node --version | awk '{print substr($1,2,2)}')
#	echo "nodejs successfully installed. Version: $node_version"
#else
#	node_version=$(node --version | awk '{print substr($1,2,2)}')
#	echo "You already have nodejs installed. Version: $node_version"
#fi

# Ensure npm installation
#if [ $is_npm = '' ]; then
#	echo "You don't have npm installed. Installing..."
#	apt-get update > /dev/null
#	apt-get install -y npm
#	sleep 1
#	npm_version=$(npm --version)
#	echo "npm successfully installed. Version: $npm_version"
#else
#	npm_version=$(npm --version)
#	echo "You already have npm installed. Version: $npm_version"
#fi

# Create new user for myapp
useradd -ms /bin/bash $NODE_USER
read -p "How do you want to call your log folder: " log_folder

function node_app() {
	# Curl the package to use
	cd ~
	tgz_file="node_envvars_project.tgz"
	curl https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz --output "$tgz_file"
	tar zxvf "$tgz_file"
  cd package
	# Set environment variables
	export APP_ENV=dev
	export DB_USER=myuser
	export DB_PWD=mysecret
  # Run the application
  log_directory="$(pwd)/$log_folder"

  if [ -d $log_directory ]; then
	  export LOG_DIR=$log_directory
	  echo "Your LOG_DIR is configured for logging"
  else
	  mkdir $log_directory
	  export LOG_DIR=$log_directory
	  echo "Your LOG_DIR is configured for logging"
  fi
  
	npm install
  node server.js & # & sign is used to run a process in the background
}

export -f node_app
su $NODE_USER -c "bash -c node_app &"
ss -ltnp | grep :3000

