#!/bin/bash

echo "Setup and configure server"

echo "all params: $*"
echo "number of params: $#"

file_name=config.yaml
config_dir=$1
config_files=$(ls "$config_dir")

# Conditionals
if [ -d "$config_dir" ]
then
	echo "reading config directory contents"
	echo "$config_files"
else
	echo "configuring dir not found. Creating config directory and files..."
	mkdir "$config_dir" && touch "$config_dir"/"$config_dir"1.sh "$config_dir"/"$config_dir"2.sh
	sleep 2
	echo "$config_files"
	echo "now config dir list"
	ls -l "$config_dir"
fi


# For Loop
for param in $*
do
  echo $param
done


sum=0
max_score=100
while true
do
	read -p "enter a score:" score
	sum=$(($sum+$score))
	echo "total score: $sum"
	
	if [ $sum -gt 100 ]
	then
		echo "You got a total Score of $sum. The program finishes now..." 
		break
	fi		
done
